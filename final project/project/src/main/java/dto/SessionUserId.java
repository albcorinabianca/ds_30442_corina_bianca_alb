package dto;

public class SessionUserId {

	private String userId;
	private static SessionUserId sessionUserId;

	public SessionUserId() {

	}

	public static SessionUserId getInstance() {
		if (sessionUserId == null) {
			sessionUserId = new SessionUserId();
		}
		return sessionUserId;
	}

	public String getUsername() {
		return userId;
	}

	public void setUsername(String userId) {
		this.userId = userId;
	}

}
