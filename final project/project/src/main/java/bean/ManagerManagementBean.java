package bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import dto.EventDto;
import dto.RequestDto;
import service.EmployeeService;
import service.EmployeeServiceService;
import service.ManagerService;
import service.ManagerServiceService;

@ManagedBean
@RequestScoped
public class ManagerManagementBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String employeeId;
	private RequestDto requestToRemove;

	private ArrayList<RequestDto> requests;
	private ArrayList<EventDto> employeeDaysOff;
	private ArrayList<String> empls;

	private ManagerService webServiceManager;
	private EmployeeService webServiceEmployee;

	@PostConstruct
	public void init() {
		webServiceManager = new ManagerServiceService().getManagerServicePort();
		webServiceEmployee = new EmployeeServiceService().getEmployeeServicePort();
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public ArrayList<String> getEmpls() {
		empls = new ArrayList<>();
		
		String employeesFromDb = webServiceManager.getAllEmployees();
		
		if (!employeesFromDb.isEmpty()) {
			String[] emplResult = employeesFromDb.split("\\&");
			for (int x = 0; x < emplResult.length; x++) {
				String[] emplAttribute = emplResult[x].split("\\|");

				empls.add(emplAttribute[1]);
			}
		}
		
		return empls;
	}
	
	public RequestDto getRequestToRemove() {
		return requestToRemove;
	}

	public void setRequestToRemove(RequestDto requestToRemove) {
		this.requestToRemove = requestToRemove;
	}

	public ArrayList<RequestDto> getRequests() {
		requests = new ArrayList<>();

		String requestsFromDb = webServiceManager.getRequests();

		if (!requestsFromDb.isEmpty()) {
			String[] requestsResult = requestsFromDb.split("\\&");
			for (int x = 0; x < requestsResult.length; x++) {
				String[] requestAttribute = requestsResult[x].split("\\|");

				RequestDto eventRequest = new RequestDto();

				eventRequest.setRequestId(requestAttribute[0]);
				eventRequest.setReason(requestAttribute[1]);
				eventRequest.setEmployeeName(requestAttribute[2]);
				eventRequest.setEmployeeId(requestAttribute[3]);
				eventRequest.setStartDate(requestAttribute[4]);
				eventRequest.setEndDate(requestAttribute[5]);

				requests.add(eventRequest);
			}
		}

		return requests;
	}

	public ArrayList<EventDto> getEmployeesDaysOff() {
		employeeDaysOff = new ArrayList<>();
		
		System.out.println(employeeId);
		
		if (employeeId != null) {
			String employeeEventsFromDb = webServiceEmployee.getEmployeeDaysOff(new Long(employeeId));

			if (!employeeEventsFromDb.isEmpty()) {
				String[] requestsResult = employeeEventsFromDb.split("\\&");
				for (int x = 0; x < requestsResult.length; x++) {
					String[] requestAttribute = requestsResult[x].split("\\|");

					EventDto event = new EventDto();

					event.setReason(requestAttribute[0]);
					event.setStartDate(requestAttribute[1]);
					event.setEndDate(requestAttribute[2]);

					employeeDaysOff.add(event);
				}
			}
		}
		
		return employeeDaysOff;
	}

	public String acceptRequest() {
		webServiceManager.acceptRequest(requestToRemove.getEmployeeId(), requestToRemove.getRequestId());

		return "manager";
	}

	public String rejectRequest() {
		webServiceManager.rejectRequest(requestToRemove.getRequestId());

		return "manager";
	}
}
