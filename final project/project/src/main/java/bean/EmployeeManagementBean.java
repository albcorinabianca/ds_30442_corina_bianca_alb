package bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import dto.EventDto;
import dto.RequestDto;
import dto.SessionUserId;
import service.EmployeeService;
import service.EmployeeServiceService;

@ManagedBean
@RequestScoped
public class EmployeeManagementBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String, String> reasons;
	private String reason;
	private Date startDate;
	private Date endDate;
	private RequestDto requestToRemove;

	private ArrayList<EventDto> employeeDaysOff;
	private ArrayList<RequestDto> employeeRequests;

	private EmployeeService webServiceEmployee;

	@PostConstruct
	public void init() {
		reasons = new HashMap<>();
		reasons.put("ill", "ILL");
		reasons.put("rest", "REST");
		reasons.put("study", "STUDY");

		webServiceEmployee = new EmployeeServiceService().getEmployeeServicePort();
	}

	public Map<String, String> getReasons() {
		return reasons;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public RequestDto getRequestToRemove() {
		return requestToRemove;
	}

	public void setRequestToRemove(RequestDto requestToRemove) {
		this.requestToRemove = requestToRemove;
	}

	public ArrayList<EventDto> getEmployeeDaysOff() {
		employeeDaysOff = new ArrayList<>();

		Long employeeId = webServiceEmployee.getEmployeeIdByName(SessionUserId.getInstance().getUsername());
		if (employeeId != null) {
			String employeeEventsFromDb = webServiceEmployee.getEmployeeDaysOff(employeeId);

			if (!employeeEventsFromDb.isEmpty()) {
				String[] requestsResult = employeeEventsFromDb.split("\\&");
				for (int x = 0; x < requestsResult.length; x++) {
					String[] requestAttribute = requestsResult[x].split("\\|");

					EventDto event = new EventDto();

					event.setReason(requestAttribute[0]);
					event.setStartDate(requestAttribute[1]);
					event.setEndDate(requestAttribute[2]);
					
					employeeDaysOff.add(event);
				}
			}
		}
		
		return employeeDaysOff;
	}

	public ArrayList<RequestDto> getEmployeeRequests() {
		employeeRequests = new ArrayList<>();
		
		Long employeeId = webServiceEmployee.getEmployeeIdByName(SessionUserId.getInstance().getUsername());
		if (employeeId != null) {
			String employeeRequestsFromDb = webServiceEmployee.getEmployeeRequests(employeeId);

			if (!employeeRequestsFromDb.isEmpty()) {
				String[] requestsResult = employeeRequestsFromDb.split("\\&");
				for (int x = 0; x < requestsResult.length; x++) {
					String[] requestAttribute = requestsResult[x].split("\\|");

					RequestDto eventRequest = new RequestDto();

					eventRequest.setReason(requestAttribute[0]);
					eventRequest.setStartDate(requestAttribute[1]);
					eventRequest.setEndDate(requestAttribute[2]);
					eventRequest.setRequestId(requestAttribute[3]);
					
					eventRequest.setEmployeeId("");
					eventRequest.setEmployeeName("");

					employeeRequests.add(eventRequest);
				}
			}
		}
		
		return employeeRequests;
	}

	public String addRequestDayOff() throws DatatypeConfigurationException {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(startDate);
		XMLGregorianCalendar startDate2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

		c.setTime(endDate);
		XMLGregorianCalendar endDate2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

		webServiceEmployee.addRequest(SessionUserId.getInstance().getUsername(), startDate2, endDate2, reason);

		return "employee";
	}

	public String removeRequestDayOff() {
		webServiceEmployee.removeRequest(requestToRemove.getRequestId());

		return "employee";
	}

}
