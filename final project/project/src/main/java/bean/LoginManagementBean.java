package bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
//import javax.faces.context.FacesContext;

import dto.SessionUserId;
import service.LoginService;
import service.LoginServiceService;

@ManagedBean
@RequestScoped
public class LoginManagementBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SessionUserId sessionUserId;
	
	private String username;
	private String password;

	@PostConstruct
	public void init() {
		
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String doLogin() {
		LoginService webServiceLogin = new LoginServiceService().getLoginServicePort();

		String role = webServiceLogin.login(username, password);
		
		if (role.equals("ADMIN")) {
			return "administrator";	
		} else if (role.equals("EMPLOYEE")) {
			sessionUserId.getInstance().setUsername(username);
			return "employee";	
		}  else if (role.equals("MANAGER")) {
			return "manager";
		}
		
		return "login";
	}

}
