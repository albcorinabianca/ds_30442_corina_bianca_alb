package bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import dto.LegalDayOffDto;
import dto.UserDto;
import service.AdministratorService;
import service.AdministratorServiceService;

@ManagedBean
@RequestScoped
public class AdministratorManagementBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String username;
	private String role;
	private Map<String, String> roles;
	private ArrayList<UserDto> users;
	private ArrayList<LegalDayOffDto> events;

	private Date date;
	private String eventDescription;

	private LegalDayOffDto eventDescriptionToRemove;

	private AdministratorService webServiceAdministrator;

	@PostConstruct
	public void init() {
		roles = new HashMap<>();
		roles.put("administroator", "ADMIN");
		roles.put("employee", "EMPLOYEE");
		roles.put("manager", "MANAGER");

		webServiceAdministrator = new AdministratorServiceService().getAdministratorServicePort();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Map<String, String> getRoles() {
		return roles;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public LegalDayOffDto getEventDescriptionToRemove() {
		return eventDescriptionToRemove;
	}

	public void setEventDescriptionToRemove(LegalDayOffDto eventDescriptionToRemove) {
		this.eventDescriptionToRemove = eventDescriptionToRemove;
	}

	public String addUser() {
		webServiceAdministrator.addUser(username, role, role);
		return "administrator";
	}

	public ArrayList<UserDto> getUsers() {
		users = new ArrayList<>();

		String employees = webServiceAdministrator.getAllEmployees();

		if (!employees.isEmpty()) {
			String[] userResult = employees.split("\\&");
			for (int x = 0; x < userResult.length; x++) {
				String[] userAttribute = userResult[x].split("\\|");

				UserDto user = new UserDto();

				user.setId(userAttribute[0]);
				user.setUsername(userAttribute[1]);
				user.setRole(userAttribute[2]);

				users.add(user);
			}
		}

		return users;
	}

	public String addLegalDayOff() throws DatatypeConfigurationException {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

		webServiceAdministrator.addEvent(eventDescription, date2);

		return "administrator";
	}

	public ArrayList<LegalDayOffDto> getEvents() {
		events = new ArrayList<>();

		String allEvents = webServiceAdministrator.getAllEvents();

		if (!allEvents.isEmpty()) {
			String[] eventResult = allEvents.split("\\&");
			for (int x = 0; x < eventResult.length; x++) {
				String[] eventAttribute = eventResult[x].split("\\|");

				LegalDayOffDto event = new LegalDayOffDto();

				event.setDate(eventAttribute[0]);
				event.setEventDescription(eventAttribute[1]);

				events.add(event);
			}
		}

		return events;
	}

	public String removeEvent() {
		webServiceAdministrator.removeEvent(eventDescriptionToRemove.getEventDescription());

		return "administrator";
	}
}
