package dao;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entity.CalendarLegalDaysOff;

public class CalendarLegalDaysOffDao {

	private SessionFactory sessionFactory;
	private static CalendarLegalDaysOffDao calendarLegalDaysOffDao;

	public CalendarLegalDaysOffDao() {
		SQLDatabaseConnection databaseSession = SQLDatabaseConnection.getInstance();
		sessionFactory = databaseSession.getSessionConnection();
	}

	public static CalendarLegalDaysOffDao getInstance() {
		if (calendarLegalDaysOffDao == null) {
			calendarLegalDaysOffDao = new CalendarLegalDaysOffDao();
		}
		return calendarLegalDaysOffDao;
	}
	
	public boolean saveLegalDayOff(CalendarLegalDaysOff calendarLegalDaysOff) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.save(calendarLegalDaysOff);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public boolean updateLegalDayOff(CalendarLegalDaysOff calendarLegalDaysOff) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.update(calendarLegalDaysOff);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public List<CalendarLegalDaysOff> findAllLegalDaysOff() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<CalendarLegalDaysOff> legalDaysOff = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM CalendarLegalDaysOff");
			legalDaysOff = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return legalDaysOff;
	}

	public CalendarLegalDaysOff findCalendarLegalDaysOff(Date date) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<CalendarLegalDaysOff> clientPackages = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM CalendarLegalDaysOff WHERE date = :date");
			query.setParameter("date", date);
			clientPackages = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return null;
		} finally {
			session.close();
		}

		return clientPackages.get(0);
	}
	
	public CalendarLegalDaysOff findCalendarLegalDaysOffEvent(String event) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<CalendarLegalDaysOff> clientPackages = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM CalendarLegalDaysOff WHERE event = :event");
			query.setParameter("event", event);
			clientPackages = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return null;
		} finally {
			session.close();
		}

		return clientPackages.get(0);
	}

	public boolean deleteLegalDayOff(String event) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		CalendarLegalDaysOff calendarLegalDayOff = findCalendarLegalDaysOffEvent(event);
		
		try {
			tx = session.beginTransaction();
			session.delete(calendarLegalDayOff);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

}
