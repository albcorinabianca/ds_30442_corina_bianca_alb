package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entity.EmployeeDaysOff;

public class EmployeeDaysOffDao {

	private static EmployeeDaysOffDao employeeDaysOffDaoInstance = null;
	private SessionFactory sessionFactory;

	public EmployeeDaysOffDao() {
		SQLDatabaseConnection databaseSession = SQLDatabaseConnection.getInstance();
		sessionFactory = databaseSession.getSessionConnection();
	}
	
	public static EmployeeDaysOffDao getInstance() {
		if (employeeDaysOffDaoInstance == null) {
			employeeDaysOffDaoInstance = new EmployeeDaysOffDao();
		}
		return employeeDaysOffDaoInstance;
	}

	public boolean saveEmployeeDayOff(EmployeeDaysOff employeeDayOff) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.save(employeeDayOff);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public List<EmployeeDaysOff> findEmployeeDaysOff(Long id) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<EmployeeDaysOff> employeesDaysOff = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM EmployeeDaysOff WHERE employeeid = :id");
			query.setParameter("id", id);
			employeesDaysOff = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return employeesDaysOff;
	}

	public EmployeeDaysOff deleteEmployeesDayOff(EmployeeDaysOff employeeDayOff) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.delete(employeeDayOff);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return employeeDayOff;
	}

}
