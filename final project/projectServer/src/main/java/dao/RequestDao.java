package dao;

import java.sql.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entity.Request;

public class RequestDao {

	private static RequestDao requestDaoInstance;
	private SessionFactory sessionFactory;

	public RequestDao() {
		SQLDatabaseConnection databaseSession = SQLDatabaseConnection.getInstance();
		sessionFactory = databaseSession.getSessionConnection();
	}

	public static RequestDao getInsance() {
		if (requestDaoInstance == null) {
			requestDaoInstance = new RequestDao();
		}
		return requestDaoInstance;
	}

	public boolean saveRequest(Request request) {
		Long id = -1l;
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			id = (Long) session.save(request);
			request.setId(id);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public List<Request> findAllRequests() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Request> requests = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Request");
			requests = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return requests;
	}

	public Request findRequestById(Long requestId) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Request> requests = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Request WHERE id = :id");
			query.setParameter("id", requestId);
			requests = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return requests.get(0);
	}
	
	public Request findRequest(String employeeName, Date dateStart, Date dateEnd) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Request> requests = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Request WHERE name = :name AND "
					+ "datestart = :datestart AND dateend = :dateend");
			query.setParameter("name", employeeName);
			query.setParameter("datestart", dateStart);
			query.setParameter("dateend", dateEnd);
			requests = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return requests.get(0);

	}

	public boolean deleteRequest(Request request) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.delete(request);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public List<Request> findRequestsForEmployeeById(Long employeeId) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Request> requests = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Request WHERE employeeId = :id");
			query.setParameter("id", employeeId);
			requests = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return requests;
	}

}
