package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entity.User;

public class UserDao {

	private static UserDao userDaoInstance = null;
	private SessionFactory sessionFactory;

	public UserDao() {
		SQLDatabaseConnection databaseSession = SQLDatabaseConnection.getInstance();
		sessionFactory = databaseSession.getSessionConnection();
	}

	public static UserDao getInstance() {
		if (userDaoInstance == null) {
			userDaoInstance = new UserDao();
		}
		return userDaoInstance;
	}

	public boolean saveUser(final User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.save(user);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public boolean updateUser(final User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.update(user);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public List<User> findAllUser() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<User> users = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User");
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return users;
	}

	public User findUser(final Long userId) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<User> user = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE id = :id");
			query.setParameter("id", userId);
			user = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return new User();
		} finally {
			session.close();
		}

		return user.get(0);
	}

	public String findUserByUsernameAndPassword(final String username, final String password) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<User> users = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE username = :username AND password = :password");
			query.setParameter("username", username);
			query.setParameter("password", password);
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return "";
		} finally {
			session.close();
		}

		if (!users.isEmpty()) {
			User user = users.get(0);
			return user.getRole();
		}
		
		return users.get(0).getRole();
	}

	public User deleteUser(final User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.delete(user);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return user;
	}

}
