package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entity.Employee;

public class EmployeeDao {

	private static EmployeeDao employeeDao;
	private SessionFactory sessionFactory;

	public EmployeeDao() {
		SQLDatabaseConnection databaseSession = SQLDatabaseConnection.getInstance();
		sessionFactory = databaseSession.getSessionConnection();
	}
	
	public static EmployeeDao getInstance() {
		if (employeeDao == null) {
			employeeDao = new EmployeeDao();
		}
		return employeeDao;
	}

	public Long saveEmployee(Employee employee) {
		Long employeeId = -1l;
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			employeeId = (Long) session.save(employee);
			employee.setId(employeeId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return 0l;
		} finally {
			session.close();
		}

		return employeeId;
	}

	public boolean updateEmployee(Employee employee) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.update(employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public List<Employee> findAllEmployees() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Employee> employees = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Employee");
			employees = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return employees;
	}

	public Employee findEmployeeById(Long employeeId) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Employee> employee = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Employee WHERE employeeid = :employeeid");
			query.setParameter("employeeid", employeeId);
			employee = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return new Employee();
		} finally {
			session.close();
		}

		return employee.get(0);
	}
	
	public Employee findEmployeeByName(String employeeName) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Employee> employee = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Employee WHERE name = :name");
			query.setParameter("name", employeeName);
			employee = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return new Employee();
		} finally {
			session.close();
		}

		return employee.get(0);
	}

	public Employee deleteEmployee(Employee employee) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.delete(employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return employee;
	}

}
