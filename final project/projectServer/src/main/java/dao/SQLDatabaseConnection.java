package dao;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class SQLDatabaseConnection {

	private static SQLDatabaseConnection connectionInstance = null;
	private SessionFactory sessionFactory;

	public SQLDatabaseConnection() {
		Configuration configuration = new Configuration();
		configuration.configure();

		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}

	public static SQLDatabaseConnection getInstance() {
		if (connectionInstance == null) {
			connectionInstance = new SQLDatabaseConnection();
		}
		return connectionInstance;
	}

	public SessionFactory getSessionConnection() {
		return sessionFactory;
	}
}
