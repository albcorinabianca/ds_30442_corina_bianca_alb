package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entity.RoleRight;

public class RoleDao {
	
	private static RoleDao roleDaoInstance = null;
	private SessionFactory sessionFactory;

	public RoleDao() {
		SQLDatabaseConnection databaseSession = SQLDatabaseConnection.getInstance();
		sessionFactory = databaseSession.getSessionConnection();
	}
	
	public static RoleDao getInstance() {
		if (roleDaoInstance == null) {
			roleDaoInstance = new RoleDao();
		}
		return roleDaoInstance;
	}

	public Long saveRole(RoleRight roleRight) {
		Long roleRightId = -1l;
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.save(roleRight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return 0l;
		} finally {
			session.close();
		}

		return roleRightId;
	}

	public boolean updateRoleRight(RoleRight roleRight) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.update(roleRight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public List<RoleRight> findAllRoleRights() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<RoleRight> roleRights = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM RoleRight");
			roleRights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return roleRights;
	}

	public RoleRight deleteRoleRight(RoleRight roleRight) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.delete(roleRight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return roleRight;
	}

}
