package controller;

import java.util.Date;
import java.util.List;

import dao.CalendarLegalDaysOffDao;
import dao.EmployeeDao;
import dao.UserDao;
import entity.CalendarLegalDaysOff;
import entity.Employee;
import entity.User;

public class AdministratorController {

	private UserDao userDao;
	private EmployeeDao employeeDao;
	private CalendarLegalDaysOffDao calendarLegalDaysOffDao;

	public AdministratorController() {
		userDao = UserDao.getInstance();
		employeeDao = EmployeeDao.getInstance();
		calendarLegalDaysOffDao = CalendarLegalDaysOffDao.getInstance();
	}

	public Long addUser(final String username, final String password, final String role) {
		Long employeeId = 0l;

		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setRole(role);

		if (role.equals("EMPLOYEE")) {
			Employee employee = new Employee();
			employee.setName(username);
			employee.setRestDaysOff(21);
			employee.setStudyDaysOff(20);

			employeeId = employeeDao.saveEmployee(employee);
		}

		userDao.saveUser(user);

		return employeeId;
	}

	public int addEvent(final String event, final Date dayOff) {
		CalendarLegalDaysOff calendarLegalDaysOff = new CalendarLegalDaysOff();
		calendarLegalDaysOff.setDayOff(dayOff);
		calendarLegalDaysOff.setEnevt(event);

		return calendarLegalDaysOffDao.saveLegalDayOff(calendarLegalDaysOff) ? 1 : 0;
	}

	public int removeEvent(final String event) {
		return calendarLegalDaysOffDao.deleteLegalDayOff(event) ? 1 : 0;
	}

	public String getAllEmployees() {
		List<User> usersFromDB = userDao.findAllUser();
		String users = "";
		for (User user : usersFromDB) {
			users += user.getId() + "|" + user.getUsername() + "|" + user.getRole() + "&";
		}

		return users;
	}

	public String getAllEvents() {
		List<CalendarLegalDaysOff> calendarLegalDaysOffFromDB = calendarLegalDaysOffDao.findAllLegalDaysOff();
		String calendarLegalDaysOff = "";
		for (CalendarLegalDaysOff calendarLegalDayOff : calendarLegalDaysOffFromDB) {
			calendarLegalDaysOff += calendarLegalDayOff.getDayOff() + "|" + calendarLegalDayOff.getEvent() + "&";
		}

		return calendarLegalDaysOff;
	}
}
