package controller;

import dao.UserDao;

public class LoginController {

	private UserDao userDao;

	public LoginController() {
		userDao = UserDao.getInstance();
	}
	
	public String login(final String username, final String password) {
		return userDao.findUserByUsernameAndPassword(username, password);
	}
	
}
