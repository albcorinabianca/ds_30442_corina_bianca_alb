package controller;

import java.util.List;

import dao.EmployeeDao;
import dao.EmployeeDaysOffDao;
import dao.RequestDao;
import entity.Employee;
import entity.EmployeeDaysOff;
import entity.Request;

public class ManagerController {

	private EmployeeDaysOffDao employeeDaysOffDao;
	private EmployeeDao employeeDao;
	private RequestDao requestDao;

	public ManagerController() {
		employeeDaysOffDao = EmployeeDaysOffDao.getInstance();
		requestDao = RequestDao.getInsance();
		employeeDao = EmployeeDao.getInstance();
	}

	public String getEmployeeDaysOff(String employeeId) {
		Long id = Long.parseLong(employeeId);
		String employeeDaysOffTransfer = "";
		List<EmployeeDaysOff> employeeDaysOff = employeeDaysOffDao.findEmployeeDaysOff(id);

		for (EmployeeDaysOff emloyeeDayOff : employeeDaysOff) {
			employeeDaysOffTransfer += emloyeeDayOff.getDayOffStart() + "|" + emloyeeDayOff.getDayOffEnd() + "|"
					+ emloyeeDayOff.getReason() + "&";
		}
		return employeeDaysOffTransfer;
	}

	public String getAllEmployees() {
		String employees = "";
		List<Employee> empls = employeeDao.findAllEmployees();
		for (Employee empl : empls) {
			employees += empl.getName() + "|" + empl.getId() + "&";
		}

		return employees;
	}

	public String getRequests() {
		String requests = "";
		List<Request> requestsDaysOff = requestDao.findAllRequests();

		for (Request request : requestsDaysOff) {
			requests += request.getId() + "|" + request.getReason() + "|" + request.getEmployee().getName() + "|"
					+ request.getEmployee().getId() + "|" + request.getDateStart() + "|" + request.getDateEnd() + "&";
		}
		return requests;
	}

	public int acceptRequest(String employeeId, String requestId) {

		if (!employeeId.isEmpty() && !requestId.isEmpty()) {
			Long eId = new Long(employeeId);
			Long rId = new Long(requestId);
			
			Request request = requestDao.findRequestById(rId);

			Employee employee = employeeDao.findEmployeeById(eId);

			EmployeeDaysOff employeeDayOff = new EmployeeDaysOff();
			employeeDayOff.setEmployee(employee);
			employeeDayOff.setReason(request.getReason());
			employeeDayOff.setDayOffEnd(request.getDateEnd());
			employeeDayOff.setDayOffStart(request.getDateStart());

			return employeeDaysOffDao.saveEmployeeDayOff(employeeDayOff) && requestDao.deleteRequest(request) ? 1 : 0;
		}

		return 0;
	}

	public int rejectRequest(String requestId) {
		if (!requestId.isEmpty()) {
			Long rId = new Long(requestId);
			Request request = requestDao.findRequestById(rId);
			return requestDao.deleteRequest(request) ? 1 : 0;
		}

		return 0;
	}
}
