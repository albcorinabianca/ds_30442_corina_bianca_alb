package controller;

import java.util.Date;
import java.util.List;

import dao.EmployeeDao;
import dao.EmployeeDaysOffDao;
import dao.RequestDao;
import entity.Employee;
import entity.EmployeeDaysOff;
import entity.Request;

public class EmployeeController {

	private EmployeeDao employeeDao;
	private EmployeeDaysOffDao employeeDaysOffDao;
	private RequestDao requestDao;

	public EmployeeController() {
		employeeDaysOffDao = EmployeeDaysOffDao.getInstance();
		employeeDao = EmployeeDao.getInstance();
		requestDao = RequestDao.getInsance();
	}

	public int addRequest(final String employeeName, final Date dateStart, final Date dateEnd, final String reason) {
		Employee employee = employeeDao.findEmployeeByName(employeeName);

		Request request = new Request();
		request.setEmployee(employee);
		request.setDateStart(new java.sql.Date(dateStart.getTime()));
		request.setDateEnd(new java.sql.Date(dateEnd.getTime()));
		request.setReason(reason);

		return requestDao.saveRequest(request) ? 1 : 0;
	}

	public String getEmployeeRequests(Long employeeId) {
		String employeeRequests = "";
		List<Request> employeeRequestsList = requestDao.findRequestsForEmployeeById(employeeId);

		for (Request request : employeeRequestsList) {
			employeeRequests += request.getReason() + "|" + request.getDateStart() + "|" + request.getDateEnd() + "|"
					+ request.getId() + "&";
		}

		return employeeRequests;
	}

	public String getEmployeeDaysOff(Long employeeId) {
		String employeeDaysOffTransfer = "";
		List<EmployeeDaysOff> employeeDaysOff = employeeDaysOffDao.findEmployeeDaysOff(employeeId);

		for (EmployeeDaysOff employeeDayOff : employeeDaysOff) {
			employeeDaysOffTransfer += employeeDayOff.getReason() + "|" + employeeDayOff.getDayOffStart() + "|"
					+ employeeDayOff.getDayOffEnd() + "&";
		}
		return employeeDaysOffTransfer;
	}

	public int removeRequest(String requestId) {
		Request request = requestDao.findRequestById(new Long(requestId));
		return requestDao.deleteRequest(request) ? 1 : 0;
	}

	public Long getEmployeeIdByName(String employeeName) {
		return employeeDao.findEmployeeByName(employeeName).getId();
	}

}
