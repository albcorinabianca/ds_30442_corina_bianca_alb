package service;

import java.util.Date;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import controller.EmployeeController;

@WebService
@SOAPBinding(style = Style.RPC)
public class EmployeeService {

	private EmployeeController employeeController;

	public EmployeeService() {
		employeeController = new EmployeeController();
	}

	public int addRequest(final String employeeName, final Date dateStart, final Date dateEnd, final String reason) {
		return employeeController.addRequest(employeeName, dateStart, dateEnd, reason);
	}

	public int removeRequest(final String requestId) {
		return employeeController.removeRequest(requestId);
	}

	public String getEmployeeDaysOff(Long id) {
		return employeeController.getEmployeeDaysOff(id);
	}
	
	public String getEmployeeRequests(Long employeeId) {
		return employeeController.getEmployeeRequests(employeeId);
	}

	public Long getEmployeeIdByName(String employeeName) {
		return employeeController.getEmployeeIdByName(employeeName);
	}
}
