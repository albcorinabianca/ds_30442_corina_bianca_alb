package service;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import controller.ManagerController;

@WebService
@SOAPBinding(style = Style.RPC)
public class ManagerService {

	private ManagerController managerController;

	public ManagerService() {
		managerController = new ManagerController();
	}

	public String getEmployeeDaysOff(String id) {
		return managerController.getEmployeeDaysOff(id);
	}

	public String getAllEmployees() {
		return managerController.getAllEmployees();
	}

	public String getRequests() {
		return managerController.getRequests();
	}

	public int acceptRequest(String employeeId, String requestId) {
		return managerController.acceptRequest(employeeId, requestId);
	}

	public int rejectRequest(String requestId) {
		return managerController.rejectRequest(requestId);
	}
}
