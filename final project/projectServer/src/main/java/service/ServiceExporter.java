package service;

import javax.xml.ws.Endpoint;

public class ServiceExporter {

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8000/service/login", new LoginService());
		Endpoint.publish("http://localhost:8000/service/manager", new ManagerService());
		Endpoint.publish("http://localhost:8000/service/administrator", new AdministratorService());
		Endpoint.publish("http://localhost:8000/service/employee", new EmployeeService());
	}

}
