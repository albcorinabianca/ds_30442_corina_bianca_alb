package service;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import controller.LoginController;

@WebService
@SOAPBinding(style = Style.RPC)
public class LoginService {

	private LoginController loginController;

	public LoginService() {
		loginController = new LoginController();
	}

	public String login(final String username, final String password) {
		return loginController.login(username, password);
	}

}
