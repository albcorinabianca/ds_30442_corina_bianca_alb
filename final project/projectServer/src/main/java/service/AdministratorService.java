package service;

import java.util.Date;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import controller.AdministratorController;

@WebService
@SOAPBinding(style = Style.RPC)
public class AdministratorService {

	private AdministratorController administratorController;

	public AdministratorService() {
		administratorController = new AdministratorController();
	}

	public Long addUser(final String username, final String password, final String role) {
		return administratorController.addUser(username, password, role);
	}

	public int addEvent(final String event, final Date dayOff) {
		return administratorController.addEvent(event, dayOff);
	}

	public int removeEvent(final String event) {
		return administratorController.removeEvent(event);
	}
	
	public String getAllEmployees() {
		return administratorController.getAllEmployees();
	}

	public String getAllEvents() {
		return administratorController.getAllEvents();
	}
}
