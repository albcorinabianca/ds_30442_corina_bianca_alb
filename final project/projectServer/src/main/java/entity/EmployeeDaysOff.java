package entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employeedaysoff")
public class EmployeeDaysOff {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "dayoffstart", nullable = false)
	private Date dayOffStart;
	@Column(name = "dayoffend", nullable = false)
	private Date dayOffEnd;
	@Column(name = "reason", nullable = false)
	private String reason;
	
	@ManyToOne
	@JoinColumn(name = "employeeid", nullable = false)
	private Employee employee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDayOffStart() {
		return dayOffStart;
	}

	public void setDayOffStart(Date dayOffStart) {
		this.dayOffStart = dayOffStart;
	}

	public Date getDayOffEnd() {
		return dayOffEnd;
	}

	public void setDayOffEnd(Date dayOffEnd) {
		this.dayOffEnd = dayOffEnd;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
