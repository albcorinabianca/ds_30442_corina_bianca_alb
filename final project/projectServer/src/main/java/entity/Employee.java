package entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "employeeid")
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "restdaysoff", nullable = false)
	private int restDaysOff;
	@Column(name = "studydaysoff", nullable = false)
	private int studyDaysOff;
	
	@OneToMany(mappedBy = "employee")
	private Set<EmployeeDaysOff> employeeDaysOff;

	@OneToMany(mappedBy = "employee")
	private Set<Request> employeeRequests;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRestDaysOff() {
		return restDaysOff;
	}

	public void setRestDaysOff(int restDaysOff) {
		this.restDaysOff = restDaysOff;
	}

	public int getStudyDaysOff() {
		return studyDaysOff;
	}

	public void setStudyDaysOff(int studyDaysOff) {
		this.studyDaysOff = studyDaysOff;
	}

	public Set<EmployeeDaysOff> getEmployeeDaysOff() {
		return employeeDaysOff;
	}

	public void setEmployeeDaysOff(Set<EmployeeDaysOff> employeeDaysOff) {
		this.employeeDaysOff = employeeDaysOff;
	}

}
