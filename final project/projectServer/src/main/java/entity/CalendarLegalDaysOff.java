package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "calendarlegaldaysoff")
public class CalendarLegalDaysOff {

	@Id
	@Column(name = "date")
	private Date date;
	
	@Column(name = "event", nullable = false)
	private String event;

	public Date getDayOff() {
		return date;
	}

	public void setDayOff(Date dayOff) {
		this.date = dayOff;
	}

	public String getEvent() {
		return event;
	}

	public void setEnevt(String event) {
		this.event = event;
	}

}
