package dto.builder;

import java.sql.Date;

import dto.CalendarLegalDaysOffDto;

public class CalendarLegalDaysOffBuilder {

	private CalendarLegalDaysOffDto calendarLegalDaysOff;

	public CalendarLegalDaysOffBuilder() {
		this.calendarLegalDaysOff = new CalendarLegalDaysOffDto();
	}

	public CalendarLegalDaysOffBuilder setDayOff(Date dayOff) {
		calendarLegalDaysOff.setDayOff(dayOff);
		return this;
	}

	public CalendarLegalDaysOffBuilder setEvent(String event) {
		calendarLegalDaysOff.setEvent(event);
		return this;
	}

	public CalendarLegalDaysOffDto build() {
		return calendarLegalDaysOff;
	}
	
}
