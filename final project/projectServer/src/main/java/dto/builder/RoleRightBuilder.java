package dto.builder;

import dto.RoleRightDto;

public class RoleRightBuilder {

	private RoleRightDto roleRight;

	public RoleRightBuilder() {
		this.roleRight = new RoleRightDto();
	}

	public RoleRightBuilder setRole(String role) {
		roleRight.setRole(role);
		return this;
	}

	public RoleRightBuilder setAction(String action) {
		roleRight.setAction(action);
		return this;
	}

	public RoleRightDto build() {
		return roleRight;
	}
}
