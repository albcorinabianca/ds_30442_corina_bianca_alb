package dto.builder;

import dto.EmployeeDto;

public class EmployeeBuilder {

	private EmployeeDto employee;
	
	public EmployeeBuilder() {
		this.employee = new EmployeeDto();
	}

	public EmployeeBuilder setName(String name) {
		employee.setName(name);
		return this;
	}

	public EmployeeBuilder setRole(String role) {
		employee.setRole(role);
		return this;
	}

	public EmployeeDto build() {
		return employee;
	}
	
}
