package dto.builder;

import dto.UserDto;

public class UserBuilder {

	private UserDto user;

	public UserBuilder() {
		this.user= new UserDto();
	}

	public UserBuilder setUsername(String username) {
		user.setUsername(username);
		return this;
	}

	public UserBuilder setPassword(String password) {
		user.setPassword(password);
		return this;
	}

	public UserBuilder setRole(String role) {
		user.setRole(role);
		return this;
	}
	
	public UserDto build() {
		return user;
	}

}
