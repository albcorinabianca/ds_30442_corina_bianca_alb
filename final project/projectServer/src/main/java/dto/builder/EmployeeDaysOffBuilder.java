package dto.builder;

import java.sql.Date;

import dto.EmployeeDaysOffDto;

public class EmployeeDaysOffBuilder {

	private EmployeeDaysOffDto employeeDaysOff;

	public EmployeeDaysOffBuilder() {
		this.employeeDaysOff = new EmployeeDaysOffDto();
	}

	public EmployeeDaysOffBuilder setDayOff(Date dayOff) {
		employeeDaysOff.setDayOff(dayOff);
		return this;
	}

	public EmployeeDaysOffBuilder setReason(String reason) {
		employeeDaysOff .setReason(reason);
		return this;
	}
	
	public EmployeeDaysOffDto build() {
		return employeeDaysOff;
	}

}
