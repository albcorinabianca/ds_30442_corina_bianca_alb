package dto;

import java.sql.Date;

public class EmployeeDaysOffDto {

	private Date dayOff;
	private String reason;

	public Date getDayOff() {
		return dayOff;
	}

	public void setDayOff(Date dayOff) {
		this.dayOff = dayOff;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
