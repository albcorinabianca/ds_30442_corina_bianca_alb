package dto;

import java.sql.Date;

public class CalendarLegalDaysOffDto {

	private Date dayOff;
	private String event;

	public Date getDayOff() {
		return dayOff;
	}

	public void setDayOff(Date dayOff) {
		this.dayOff = dayOff;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

}
