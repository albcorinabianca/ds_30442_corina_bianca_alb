package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import service.Client;
import view.ClientPanel;

public class ClientViewCotroller {

	private ClientPanel client;
	private Client webServiceClient;
	private String username;
	
	private Long packageId;

	public ClientViewCotroller(String username, Client webServiceClient) {
		this.webServiceClient = webServiceClient; 
		this.username = username;
		
		client = new ClientPanel("client");
		client.setVisible(true);

		client.searchButtonActionListener(new SearchActionListener());
		client.checkStatusButtonActionListener(new CheckStatusActionListener());
		client.allPackagesButtonActionListener(new AllPackagesActionListener());
	}

	class SearchActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			packageId = Long.parseLong(client.getPackageId());
					
			String pack = webServiceClient.getPackageById(packageId);
			System.out.println("SEARCH: " + pack);
			client.clear();
			client.setAllPackagesText(pack);
		}

	}

	class CheckStatusActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			packageId = Long.parseLong(client.getPackageId());

			String packageRoute = webServiceClient.checkPackageStatus(packageId);
			System.out.println("CHECK " + packageRoute);
			client.clear();
			client.setAllPackagesText(packageRoute);
		}

	}

	class AllPackagesActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			String allPackages = webServiceClient.getAllClientPackages(username);
			System.out.println("ALL PACKAGES: " + allPackages);
			client.setAllPackagesText(allPackages);
		}

	}
}
