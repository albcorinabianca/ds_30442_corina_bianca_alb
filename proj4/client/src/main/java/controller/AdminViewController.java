package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import service.Administrator;
import view.AdministratorPanel;

public class AdminViewController {

	private Administrator webServiceAdministrator;

	private AdministratorPanel administrator;

	private String senderClient;
	private String receiverClinet;
	private String packageName;
	private String description;
	private String senderCity;
	private String destinationCity;

	private Long packageId;

	private String routeCity;
	private String routeTime;
	private String routePackageId;

	public AdminViewController(Administrator webServiceAdministrator) {
		this.webServiceAdministrator = webServiceAdministrator;

		administrator = new AdministratorPanel("administrator");
		administrator.setVisible(true);

		administrator.addPackageButtonActionListener(new AddPackageActionListener());
		administrator.packageStatusUpdateButtonActionListener(new PackageStatusUpdateActionListener());
		administrator.registerPackageButtonActionListener(new RegisterPackageActionListener());
		administrator.removePackageButtonActionListener(new RemovePackageActionListener());
	}

	class AddPackageActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			senderClient = administrator.getSender();
			receiverClinet = administrator.getReceiver();
			packageName = administrator.getPackageName();
			description = administrator.getDescription();
			senderCity = administrator.getSenderCity();
			destinationCity = administrator.getDestinationCity();

			Long packageId = webServiceAdministrator.addPackage(senderClient, receiverClinet, packageName, description,
					senderCity, destinationCity);

			administrator.showAddedDialog(packageId);
		}

	}

	class PackageStatusUpdateActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			routeCity = administrator.getCityStatus();
			routeTime = administrator.getTimeStatus();
			routePackageId = administrator.getRoutePackageId();

			webServiceAdministrator.addRoute(routeCity, routeTime, Long.parseLong(routePackageId));
			administrator.showAddRouteDialog();
		}

	}

	class RegisterPackageActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			packageId = Long.parseLong(administrator.getPackageId());

			webServiceAdministrator.trackPackage(packageId);
			administrator.showTrackRouteDialog(packageId);
		}

	}

	class RemovePackageActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			packageId = Long.parseLong(administrator.getPackageId());

			webServiceAdministrator.removePackage(packageId);
			administrator.showRemovedRouteDialog(packageId);
		}

	}
}
