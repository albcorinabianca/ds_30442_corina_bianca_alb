package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import service.Administrator;
import service.AdministratorService;
import service.Client;
import service.ClientService;
import view.LoginPanel;

public class LoginController {

	private Administrator webServiceAdministrator;
	private Client webServiceClient;

	private LoginPanel login;
	private String username;
	private String password;

	public LoginController() {
		webServiceAdministrator = new AdministratorService().getAdministratorPort();
		webServiceClient = new ClientService().getClientPort();

		login = new LoginPanel("login");
		login.setVisible(true);

		login.loginButtonActionListener(new LoginActionListener());
		login.registerButtonActionListener(new RegisterActionListener());
	}

	class LoginActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			username = login.getUserame();
			password = login.getPassword();

			System.out.println(
					"LOGIN " + username + " " + password + " " + webServiceAdministrator.login(username, password));

			if (webServiceAdministrator.login(username, password)) {
				login.dispose();
				new AdminViewController(webServiceAdministrator);
			} else if (webServiceClient.login(username, password)) {
				login.dispose();
				new ClientViewCotroller(username, webServiceClient);
			}
		}

	}

	class RegisterActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			System.out.println("REGISTER");
			username = login.getUserame();
			password = login.getPassword();

			System.out.println("try to register");
			if (webServiceClient.register(username, password)) {
				login.dispose();
				new ClientViewCotroller(username, webServiceClient);
			} else {
				System.out.println("failed to register");
			}
		}

	}

}
