package dto;

import java.sql.Date;

public class Route {

	private Long id;

	private Date time;
	private String city;
	private Long packageId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	@Override
	public String toString() {
		return "Route [id=" + id + ", time=" + time + ", city=" + city + ", packageId=" + packageId + "]";
	}
}
