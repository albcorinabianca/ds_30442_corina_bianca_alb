package dto;

public class ClientPackage {

	private Long id;

	private String senderClinet;
	private String receiverClinet;
	private String packageName;
	private String description;
	private String senderCity;
	private String destinationCity;
	private Boolean tracking;

	public ClientPackage(Boolean tracking) {
		this.tracking = tracking;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSenderClinet() {
		return senderClinet;
	}

	public void setSenderClinet(String senderClinet) {
		this.senderClinet = senderClinet;
	}

	public String getReceiverClinet() {
		return receiverClinet;
	}

	public void setReceiverClinet(String receiverClinet) {
		this.receiverClinet = receiverClinet;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public Boolean getTracking() {
		return tracking;
	}

	public void setTracking(Boolean tracking) {
		this.tracking = tracking;
	}

	@Override
	public String toString() {
		return "Package [id=" + id + ", senderClinet=" + senderClinet + ", receiverClinet=" + receiverClinet
				+ ", packageName=" + packageName + ", description=" + description + ", senderCity=" + senderCity
				+ ", destinationCity=" + destinationCity + ", tracking=" + tracking + "]";
	}

}
