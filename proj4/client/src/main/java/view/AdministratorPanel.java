package view;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.LoginController;

public class AdministratorPanel extends JFrame {

	private JPanel contentPane;

	private JTextField packageId;
	private JButton removePackageButton;
	private JButton registerPackageButton;

	private JTextField cityStatusTextField;
	private JTextField timeStatusTextField;
	private JTextField packageIdStatusTextField;
	private JButton packageStatusUpdateButton;

	private JTextField senderClientTextField;
	private JTextField receiverClinetTextField;
	private JTextField packageNameTextField;
	private JTextField descriptionTextField;
	private JTextField senderCityTextField;
	private JTextField destinationCityTextField;
	private JButton addPackageButton;

	/**
	 * @param title
	 */
	public AdministratorPanel(String title) {
		setTitle(title);
		setBounds(300, 100, 450, 300);

		WindowListener exitListener = new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				new LoginController();
			}
		};
		addWindowListener(exitListener);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel packageLabel = new JLabel("PACKAGE");
		packageLabel.setBounds(10, 10, 120, 14);
		contentPane.add(packageLabel);

		JLabel senderLabel = new JLabel("sender: ");
		senderLabel.setBounds(10, 40, 120, 14);
		contentPane.add(senderLabel);

		senderClientTextField = new JTextField();
		senderClientTextField.setBounds(105, 40, 80, 20);
		contentPane.add(senderClientTextField);
		senderClientTextField.setColumns(10);

		JLabel receiverLabel = new JLabel("receiver: ");
		receiverLabel.setBounds(10, 60, 120, 14);
		contentPane.add(receiverLabel);

		receiverClinetTextField = new JTextField();
		receiverClinetTextField.setBounds(105, 60, 80, 20);
		contentPane.add(receiverClinetTextField);
		receiverClinetTextField.setColumns(10);

		JLabel packageNameLabel = new JLabel("package name: ");
		packageNameLabel.setBounds(10, 80, 120, 14);
		contentPane.add(packageNameLabel);

		packageNameTextField = new JTextField();
		packageNameTextField.setBounds(105, 80, 80, 20);
		contentPane.add(packageNameTextField);
		packageNameTextField.setColumns(10);

		JLabel descriptionLabel = new JLabel("description: ");
		descriptionLabel.setBounds(10, 100, 120, 14);
		contentPane.add(descriptionLabel);

		descriptionTextField = new JTextField();
		descriptionTextField.setBounds(105, 100, 80, 20);
		contentPane.add(descriptionTextField);
		descriptionTextField.setColumns(10);

		JLabel senderCityLabel = new JLabel("sender city: ");
		senderCityLabel.setBounds(10, 120, 120, 14);
		contentPane.add(senderCityLabel);

		senderCityTextField = new JTextField();
		senderCityTextField.setBounds(105, 120, 80, 20);
		contentPane.add(senderCityTextField);
		senderCityTextField.setColumns(10);

		JLabel destinationCityLabel = new JLabel("destination city: ");
		destinationCityLabel.setBounds(10, 140, 120, 14);
		contentPane.add(destinationCityLabel);

		destinationCityTextField = new JTextField();
		destinationCityTextField.setBounds(105, 140, 80, 20);
		contentPane.add(destinationCityTextField);
		destinationCityTextField.setColumns(10);

		addPackageButton = new JButton("add package");
		addPackageButton.setBounds(40, 170, 120, 20);
		contentPane.add(addPackageButton);

		JLabel packageIdLabel = new JLabel("package id: ");
		packageIdLabel.setBounds(200, 40, 80, 20);
		contentPane.add(packageIdLabel);

		packageId = new JTextField();
		packageId.setBounds(270, 40, 80, 20);
		contentPane.add(packageId);
		packageId.setColumns(10);

		removePackageButton = new JButton("REMOVE");
		removePackageButton.setBounds(200, 70, 90, 20);
		contentPane.add(removePackageButton);

		registerPackageButton = new JButton("track");
		registerPackageButton.setBounds(300, 70, 80, 20);
		contentPane.add(registerPackageButton);

		JLabel cityLabel = new JLabel("city: ");
		cityLabel.setBounds(200, 120, 80, 20);
		contentPane.add(cityLabel);

		cityStatusTextField = new JTextField();
		cityStatusTextField.setBounds(280, 120, 90, 20);
		contentPane.add(cityStatusTextField);
		cityStatusTextField.setColumns(10);

		JLabel timeLabel = new JLabel("time: ");
		timeLabel.setBounds(200, 140, 80, 20);
		contentPane.add(timeLabel);

		timeStatusTextField = new JTextField();
		timeStatusTextField.setBounds(280, 140, 90, 20);
		contentPane.add(timeStatusTextField);
		timeStatusTextField.setColumns(10);

		JLabel packageIdRouteLabel = new JLabel("package id: ");
		packageIdRouteLabel.setBounds(200, 160, 80, 20);
		contentPane.add(packageIdRouteLabel);

		packageIdStatusTextField = new JTextField();
		packageIdStatusTextField.setBounds(280, 160, 90, 20);
		contentPane.add(packageIdStatusTextField);
		packageIdStatusTextField.setColumns(10);

		packageStatusUpdateButton = new JButton("UPDATE track");
		packageStatusUpdateButton.setBounds(220, 190, 120, 20);
		contentPane.add(packageStatusUpdateButton);
	}

	public String getPackageId() {
		return packageId.getText();
	}

	public String getCityStatus() {
		return cityStatusTextField.getText();
	}

	public String getTimeStatus() {
		return timeStatusTextField.getText();
	}

	public String getSender() {
		return senderClientTextField.getText();
	}

	public String getReceiver() {
		return receiverClinetTextField.getText();
	}

	public String getPackageName() {
		return packageNameTextField.getText();
	}

	public String getDescription() {
		return descriptionTextField.getText();
	}

	public String getSenderCity() {
		return senderCityTextField.getText();
	}

	public String getDestinationCity() {
		return destinationCityTextField.getText();
	}

	public String getRoutePackageId() {
		return packageIdStatusTextField.getText();
	}

	public void removePackageButtonActionListener(ActionListener e) {
		removePackageButton.addActionListener(e);
	}

	public void registerPackageButtonActionListener(ActionListener e) {
		registerPackageButton.addActionListener(e);
	}

	public void packageStatusUpdateButtonActionListener(ActionListener e) {
		packageStatusUpdateButton.addActionListener(e);
	}

	public void addPackageButtonActionListener(ActionListener e) {
		addPackageButton.addActionListener(e);
	}

	public void clear() {
		packageId.setText("");
		cityStatusTextField.setText("");
		timeStatusTextField.setText("");
		senderClientTextField.setText("");
		receiverClinetTextField.setText("");
		packageNameTextField.setText("");
		descriptionTextField.setText("");
		senderCityTextField.setText("");
		destinationCityTextField.setText("");
	}

	public void showAddedDialog(Long packageId) {
		JOptionPane.showMessageDialog(this, "Package with id " + packageId + " has been added", "A plain message",
				JOptionPane.PLAIN_MESSAGE);
	}

	public void showAddRouteDialog() {
		JOptionPane.showMessageDialog(this, "Route has been added", "A plain message", JOptionPane.PLAIN_MESSAGE);
	}

	public void showRemovedRouteDialog(Long packageId) {
		JOptionPane.showMessageDialog(this, "Package with id " + packageId + " has been removed", "A plain message",
				JOptionPane.PLAIN_MESSAGE);
	}

	public void showTrackRouteDialog(Long packageId) {
		JOptionPane.showMessageDialog(this, "Package with id " + packageId + " is being tracked", "A plain message",
				JOptionPane.PLAIN_MESSAGE);
	}

}
