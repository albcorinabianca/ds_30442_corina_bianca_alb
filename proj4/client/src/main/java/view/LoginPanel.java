package view;

import java.awt.HeadlessException;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class LoginPanel extends JFrame {

	private JPanel contentPane;
	private JTextField usernameTextField;
	private JTextField passwordTextField;
	private JButton loginButton;
	private JButton registerButton;
	
	public LoginPanel(String title) throws HeadlessException {
		setTitle(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 100, 250, 170);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel usernameLable = new JLabel("username");
		usernameLable.setBounds(10, 11, 120, 14);
		contentPane.add(usernameLable);
		
		usernameTextField = new JTextField();
		usernameTextField.setBounds(80, 11, 86, 20);
		contentPane.add(usernameTextField);
		usernameTextField.setColumns(10);
		
		JLabel passwordLabel = new JLabel("password");
		passwordLabel.setBounds(10, 33, 86, 20);
		contentPane.add(passwordLabel);
		
		passwordTextField = new JTextField();
		passwordTextField.setBounds(80, 33, 86, 20);
		contentPane.add(passwordTextField);
		passwordTextField.setColumns(10);
		
		loginButton = new JButton("LOGIN");
		loginButton.setBounds(10, 70, 86, 20);
		contentPane.add(loginButton);
		
		registerButton = new JButton("REGISTER");
		registerButton.setBounds(90, 70, 100, 20);
		contentPane.add(registerButton);
	}

	public String getUserame() {
		return usernameTextField.getText();
	}
	
	public String getPassword() {
		return passwordTextField.getText();
	}
	
	public void loginButtonActionListener(ActionListener e) {
		loginButton.addActionListener(e);
	}
	
	public void registerButtonActionListener(ActionListener e) {
		registerButton.addActionListener(e);
	}

	public void clear() {
		usernameTextField.setText("");
		passwordTextField.setText("");
	}
}
