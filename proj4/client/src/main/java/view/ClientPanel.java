package view;

import java.awt.HeadlessException;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.LoginController;

public class ClientPanel extends JFrame {

	private JPanel contentPane;
	private JTextField packageIdTextField;
	private JButton searchButton;
	private JButton checkStatusButton;
	private JButton allPackagesButton;
	private JTextArea allPackagesTextArea;

	public ClientPanel(String title) throws HeadlessException {
		setTitle(title);
		setBounds(300, 100, 400, 350);
		WindowListener exitListener = new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				new LoginController();
			}
		};
		addWindowListener(exitListener);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel packageIdLable = new JLabel("package id: ");
		packageIdLable.setBounds(10, 10, 120, 14);
		contentPane.add(packageIdLable);

		packageIdTextField = new JTextField();
		packageIdTextField.setBounds(80, 10, 80, 20);
		contentPane.add(packageIdTextField);
		packageIdTextField.setColumns(10);

		searchButton = new JButton("SEARCH");
		searchButton.setBounds(10, 40, 90, 20);
		contentPane.add(searchButton);

		checkStatusButton = new JButton("STATUS");
		checkStatusButton.setBounds(100, 40, 90, 20);
		contentPane.add(checkStatusButton);

		allPackagesButton = new JButton("My PACKAGES");
		allPackagesButton.setBounds(20, 80, 120, 20);
		contentPane.add(allPackagesButton);

		allPackagesTextArea = new JTextArea();
		allPackagesTextArea.setBounds(20, 110, 340, 150);
		contentPane.add(allPackagesTextArea);

		// JScrollPane scroll = new JScrollPane(allPackagesTextArea,
		// JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
		// JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//
		// contentPane.add(scroll);
	}

	public void setAllPackagesText(String allPackages) {
		allPackagesTextArea.setText(allPackages);
	}

	public void searchButtonActionListener(ActionListener e) {
		searchButton.addActionListener(e);
	}

	public void checkStatusButtonActionListener(ActionListener e) {
		checkStatusButton.addActionListener(e);
	}

	public void allPackagesButtonActionListener(ActionListener e) {
		allPackagesButton.addActionListener(e);
	}

	public String getPackageId() {
		return packageIdTextField.getText();
	}

	public void clear() {
		packageIdTextField.setText("");
		allPackagesTextArea.setText("");
	}

}
