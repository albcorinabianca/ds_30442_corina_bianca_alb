package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entity.User;

public class UserDao {

	private SessionFactory sessionFactory;
	
	public UserDao() {
		SQLAccess databaseSession = SQLAccess.getInstance();
		sessionFactory = databaseSession.getSessionConnection();
	}

	public User saveUser(User user) {
		Long userId = -1l;
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			userId = (Long) session.save(user);
			user.setId(userId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return null;
		} finally {
			session.close();
		}

		return user;
	}

	public User updateUser(User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.update(user);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return user;
	}

	public User findUser(final String username, final String password) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<User> users = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE username = :username AND password = :password");
			query.setParameter("username", username);
			query.setParameter("password", password);
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		if (users != null) {
			return users.get(0);	
		}
		
		return null;
	}

}
