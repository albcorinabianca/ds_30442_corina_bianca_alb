package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entity.Route;

public class RouteDao {

	private SessionFactory sessionFactory;
	
	public RouteDao() {
		SQLAccess databaseSession = SQLAccess.getInstance();
		sessionFactory = databaseSession.getSessionConnection();
	}

	public Route saveRoute(Route route) {
		Long packageId = -1l;
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			packageId = (Long) session.save(route);
			route.setId(packageId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return null;
		} finally {
			session.close();
		}

		return route;
	}

	public Route updateRoute(Route route) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.update(route);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return route;
	}

	public List<Route> findRoutes(Long packageId) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Route> routes = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Route WHERE packageid = :packageid");
			query.setParameter("packageid", packageId);
			routes = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		if (!routes.isEmpty()) {
			System.out.println("DATABASE : " + routes.get(0).toString());
		}
		
		return routes;
	}

}
