package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entity.Pack;

public class PackDao {

	private SessionFactory sessionFactory;

	public PackDao() {
		SQLAccess databaseSession = SQLAccess.getInstance();
		sessionFactory = databaseSession.getSessionConnection();
	}

	public Long savePackage(Pack pack) {
		Long packageId = -1l;
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			packageId = (Long) session.save(pack);
			pack.setId(packageId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return null;
		} finally {
			session.close();
		}

		return packageId;
	}

	public boolean updatePackage(Pack clientPackage) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.update(clientPackage);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public List<Pack> findAllPackages(String packageName) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Pack> clientPackages = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM ClientPackage WHERE package = :packageName");
			query.setParameter("packageName", packageName);
			clientPackages = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return clientPackages;
	}

	public Pack findPackageById(Long packageId) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Pack> clientPackages = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Pack WHERE id = :id");
			query.setParameter("id", packageId);
			clientPackages = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return null;
		} finally {
			session.close();
		}

		return clientPackages.get(0);
	}

	public Pack findPackage(String packageName) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Pack> clientPackages = null;
		
		System.out.println("Try to find packagers!");
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Pack WHERE packagename = :packageName");
			query.setParameter("packageName", packageName);
			clientPackages = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

		return clientPackages.get(0);
	}

	public List<Pack> findClientPackage(String clientName) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Pack> clientPackages = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Pack WHERE senderclient = :sender OR receiverclient = :receiver");
			query.setParameter("sender", clientName);
			query.setParameter("receiver", clientName);
			clientPackages = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			return null;
		} finally {
			session.close();
		}

		return clientPackages;
	}

	public Pack deleteClientPackage(Long packageId) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Pack clientPackage = null;

		clientPackage = findPackageById(packageId);

		if (clientPackage != null) {
			try {
				tx = session.beginTransaction();
				session.delete(clientPackage);
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
			} finally {
				session.close();
			}
		}
		return clientPackage;
	}
}
