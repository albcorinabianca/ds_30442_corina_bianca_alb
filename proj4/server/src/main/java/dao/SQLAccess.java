package dao;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class SQLAccess {

	private static SQLAccess connectionInstance = null;
	private SessionFactory sessionFactory;

	public SQLAccess() {
		Configuration configuration = new Configuration();
		configuration.configure();

		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}

	public static SQLAccess getInstance() {
		if (connectionInstance == null) {
			connectionInstance = new SQLAccess();
		}
		return connectionInstance;
	}

	public SessionFactory getSessionConnection() {
		return sessionFactory;
	}

}
