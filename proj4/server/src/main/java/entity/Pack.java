package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pack")
public class Pack {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "senderclient", nullable = false)
	private String senderClient;
	@Column(name = "receiverclient", nullable = false)
	private String receiverClient;
	@Column(name = "packagename", nullable = false)
	private String packageName;
	@Column(name = "description", nullable = true)
	private String description;
	@Column(name = "sendercity", nullable = false)
	private String senderCity;
	@Column(name = "destinationcity", nullable = false)
	private String destinationCity;
	@Column(name = "tracking")
	private int tracking;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSenderClinet() {
		return senderClient;
	}

	public void setSenderClinet(String senderClinet) {
		this.senderClient = senderClinet;
	}

	public String getReceiverClinet() {
		return receiverClient;
	}

	public void setReceiverClient(String receiverClinet) {
		this.receiverClient = receiverClinet;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public Boolean getTracking() {
		if (tracking == 1) {
			return true;
		}
		return false;
	}

	public void setTracking(Boolean tracking) {
		if (tracking) {
			this.tracking = 1;
		} else {
			this.tracking = 0;
		}
	}

	@Override
	public String toString() {
		return "Package with id " + id + "name " + packageName + "\nand description \"" + description + "\"\nis sent by "
				+ senderClient + " from " + senderCity + "\nto " + receiverClient + " in " + destinationCity + "\n";
	}

}
