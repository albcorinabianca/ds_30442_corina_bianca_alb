package service;

import java.util.ArrayList;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import business.ClientDatabaseAccess;
import business.LoginDatabase;
import entity.Pack;

@WebService
@SOAPBinding(style = Style.RPC)
public class Client {

	private LoginDatabase loginDatabase;
	private ClientDatabaseAccess clientDatabaseAccess;

	public Client() {
		loginDatabase = new LoginDatabase();
	}

	public String sayHelloClient() {
		return "Hy from client serivce!";
	}

	public boolean login(final String username, final String password) {
		clientDatabaseAccess = (ClientDatabaseAccess) loginDatabase.login("Client", username, password);
		if (clientDatabaseAccess != null) {
			return true;
		}
		return false;
	}

	public boolean register(final String username, final String password) {
		boolean registered = loginDatabase.register(username, password);
		if (registered) {
			clientDatabaseAccess = (ClientDatabaseAccess) loginDatabase.login("Client", username, password);
			return true;
		}
		return false;
	}

	public String getPackageByName(String packageName) {
		Pack clientPackage = clientDatabaseAccess.getPackageByName(packageName);
		if (clientPackage != null) {
			return clientPackage.toString();
		}

		return null;
	}
	
	public String getPackageById(Long packageId) {
		Pack clientPackage = clientDatabaseAccess.getPackageById(packageId);
		if (clientPackage != null) {
			return clientPackage.toString();
		}

		return null;
	}

	public String checkPackageStatus(Long packageId) {
		return clientDatabaseAccess.checkPackageStatus(packageId).toString();
	}

	public String getAllClientPackages(String clientName) {
		ArrayList<Pack> clientPackages = clientDatabaseAccess.getAllClientPackages(clientName);

		if (!clientPackages.isEmpty()) {
			ArrayList<String> clientPackagesString = new ArrayList<String>();

			for (Pack clientPackage : clientPackages) {
				clientPackagesString.add(clientPackage.toString());
			}

			return clientPackagesString.toString();
		}

		return "";
	}

}
