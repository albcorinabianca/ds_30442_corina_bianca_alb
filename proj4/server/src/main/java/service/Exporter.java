package service;

import javax.xml.ws.Endpoint;

public class Exporter {

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8080/service/client", new Client());
		Endpoint.publish("http://localhost:8080/service/administrator", new Administrator());
	}

}
