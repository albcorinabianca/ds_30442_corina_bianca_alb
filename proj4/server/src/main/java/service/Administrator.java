package service;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import business.AdministratorDatabaseAccess;
import business.LoginDatabase;

@WebService
@SOAPBinding(style = Style.RPC)
public class Administrator {

	private LoginDatabase loginDatabase;
	private AdministratorDatabaseAccess administratorDatabaseAccess;

	public Administrator() {
		loginDatabase = new LoginDatabase();
	}

	public String sayHelloAdmin() {
		return "Hy from admin service!";
	}

	public boolean login(final String username, final String password) {
		administratorDatabaseAccess = (AdministratorDatabaseAccess) loginDatabase.login("Administrator", username,
				password);
		if (administratorDatabaseAccess != null) {
			return true;
		}
		return false;
	}

	public Long addPackage(final String senderClient, final String receiverClient, final String packageName,
			final String descriptor, final String senderCity, final String destinationCity) {
		System.out.println("lala : " + senderClient);
		
		return administratorDatabaseAccess.addPackage(senderClient, receiverClient, packageName, descriptor, senderCity,
				destinationCity);
	}
	
	public boolean addRoute(final String city, final String time, final Long packageId) {
		return administratorDatabaseAccess.addRoute(city, time, packageId);
	}
	
	public boolean trackPackage(Long packageId) {
		return administratorDatabaseAccess.trackPackage(packageId);
	}
	
	public boolean removePackage(Long packageId) {
		return administratorDatabaseAccess.removePackage(packageId);
	}
}
