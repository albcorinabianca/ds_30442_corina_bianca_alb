import service.Client;

public class Test {

	public static void main(String[] args) {
		Client client = new Client();
		client.register("test", "test");

		System.out.println("Show me some tracking : " + client.checkPackageStatus(2l).toString());
		System.out.println("Show me some package: " + client.getPackageById(2l).toString());
		System.out.println("Show me all my package: " + client.getAllClientPackages("we").toString());
		
//		Administrator administrator = new Administrator();
//		
//		administrator.login("user", "pass");
//		
//		Long packageId = administrator.addPackage("w", "receiverClient", "packageName", "descriptor", "senderCity", "destinationCity");
//		administrator.addRoute("Cluj-Napoca", "12:20", packageId);
//		
//		administrator.trackPackage(5l);
//		
//		administrator.removePackage(11l);
		
	}

}
