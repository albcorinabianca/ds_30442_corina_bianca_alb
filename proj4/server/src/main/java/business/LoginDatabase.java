package business;

import dao.PackDao;
import dao.RouteDao;
import dao.UserDao;
import entity.User;

public class LoginDatabase {

	private UserDao userDao;
	private PackDao packageDao;
	private RouteDao routeDao;

	public LoginDatabase() {
		userDao = new UserDao();
		packageDao = new PackDao();
		routeDao = new RouteDao();
	}

	public UserDatabaseAccess login(final String clazz, final String username, final String password) {
		User user = userDao.findUser(username, password);

		System.out.println("LOGIN: " + username + " " + password);
		
		if (user != null) {
			System.out.println("LOGIN USER " + user.toString());
			if (user.getRole().equals("ADMIN") && clazz.equals("Administrator")) {
				System.out.println("ADMIN");
				return new AdministratorDatabaseAccess(packageDao, routeDao);
			} else if (user.getRole().equals("CLIENT") && clazz.equals("Client")) {
				System.out.println("CLIENT");
				return new ClientDatabaseAccess(userDao, packageDao, routeDao);
			}
		}

		return null;
	}

	public boolean register(final String username, final String password) {
		return new ClientDatabaseAccess(userDao, packageDao, routeDao).addClient(username, password);
	}
}
