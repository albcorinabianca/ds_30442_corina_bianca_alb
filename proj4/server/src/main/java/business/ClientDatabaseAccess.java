package business;

import java.util.ArrayList;

import dao.PackDao;
import dao.RouteDao;
import dao.UserDao;
import entity.Pack;
import entity.Route;
import entity.User;

public class ClientDatabaseAccess implements UserDatabaseAccess {

	private UserDao userDao;
	private PackDao packageDao;
	private RouteDao routeDao;

	private final String CLIENT_ROLE = "CLIENT";

	public ClientDatabaseAccess(UserDao userDao, PackDao packageDao, RouteDao routeDao) {
		this.userDao = userDao;
		this.packageDao = packageDao;
		this.routeDao = routeDao;
	}

	public boolean addClient(final String username, final String password) {
		User user = new User();

		user.setUsername(username);
		user.setPassword(password);
		user.setRole(CLIENT_ROLE);

		System.out.println("ClientDatabase");

		if (userDao.saveUser(user) != null) {
			return true;
		}

		return false;
	}

	public Pack getPackageByName(String packageName) {
		Pack clientPackage = packageDao.findPackage(packageName);

		if (clientPackage != null) {
			return clientPackage;
		}

		return null;
	}
	
	public Pack getPackageById(Long packageId) {
		Pack clientPackage = packageDao.findPackageById(packageId);

		if (clientPackage != null) {
			return clientPackage;
		}

		return null;
	}

	public ArrayList<String> checkPackageStatus(Long packageId) {
		ArrayList<Route> routes = (ArrayList<Route>) routeDao.findRoutes(packageId);
		ArrayList<String> trackingStatus = new ArrayList<String>(); 
		
		if (!routes.isEmpty()) {
			System.out.println("ROUTES : " + routes.toString());

			for (Route route : routes) {
				trackingStatus.add(route.toString());
			}
		} else {
			trackingStatus.add("city = unknown, time = unknown");
		}

		System.out.println("Tracking status: " + trackingStatus.get(0).toString());

		return trackingStatus;
	}

	public ArrayList<Pack> getAllClientPackages(String clientName) {
		ArrayList<Pack> clientPackages = (ArrayList<Pack>) packageDao.findClientPackage(clientName);

		if (clientPackages != null) {
			return clientPackages;
		}

		return null;
	}
}
