package business;

import dao.PackDao;
import dao.RouteDao;
import entity.Pack;
import entity.Route;

public class AdministratorDatabaseAccess implements UserDatabaseAccess {

	private PackDao packageDao;
	private RouteDao routeDao;

	public AdministratorDatabaseAccess(final PackDao packageDao, final RouteDao routeDao) {
		this.packageDao = packageDao;
		this.routeDao = routeDao;
	}

	public Long addPackage(final String senderClient, final String receiverClient, final String packageName,
			final String description, final String senderCity, final String destinationCity) {
		Pack pack = new Pack();
		
		pack.setSenderClinet(senderClient);
		pack.setReceiverClient(receiverClient);
		pack.setPackageName(packageName);
		pack.setDescription(description);
		pack.setSenderCity(senderCity);
		pack.setDestinationCity(destinationCity);
		pack.setTracking(false);
				
		return packageDao.savePackage(pack);
	}

	public boolean addRoute(final String city, final String time, final Long packageId) {
		Route route = new Route();
		route.setCity(city);
		route.setTime(time);
		route.setPackageId(packageId);
		
		if (routeDao.saveRoute(route) != null) {
			return true;
		}
		
		return false;
	}
	
	public boolean trackPackage(Long packageId) {
		Pack pack = packageDao.findPackageById(packageId);
		
		System.out.println(pack.toString());
		
		if (pack != null) {
			pack.setTracking(true);
			
			return packageDao.updatePackage(pack);
		}
		
		return false;
	}
	
	public boolean removePackage(Long packageId) {
		if (packageDao.deleteClientPackage(packageId) != null) {
			return true;
		}
		
		return false;
	}
}
